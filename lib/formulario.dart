import 'package:flutter/material.dart';

class Formularinho extends StatefulWidget {
  const Formularinho({super.key});

  @override
  State<Formularinho> createState() => _FormularinhoState();
}

class _FormularinhoState extends State<Formularinho> {
  double _sbh = 20.0; // variavel para tamanho de espaçamento entre campos

  //controladores para capturar textos dos TextFields
  TextEditingController _celsiusController = TextEditingController();
  TextEditingController _fahrenheitController = TextEditingController();
  TextEditingController _kelvinController = TextEditingController();


  void _Calcular(){

    //conjunto condicional para checar se o campo Não esta vazio E É valido
    //caso nenhuma condição seja atendida, cria uma modal com algum aviso

    if(_celsiusController.text.isNotEmpty && double.tryParse(_celsiusController.text)!=null){
      double _celsius =  double.parse(_celsiusController.text);
      double _fahrenheit = (_celsius*1.8)+32;
      double _kelvin = _celsius+273;

      _fahrenheitController.text = _fahrenheit.toString();
      _kelvinController.text = _kelvin.toString();
    }else if(_fahrenheitController.text.isNotEmpty && double.tryParse(_fahrenheitController.text)!=null){
      double _fahrenheit = double.parse(_fahrenheitController.text);
      double _celsius =  (_fahrenheit-32)/1.8;
      double _kelvin = (_fahrenheit-32)*5/9+273;

      _celsiusController.text = _celsius.toString();
      _kelvinController.text = _kelvin.toString();
    }else if(_kelvinController.text.isNotEmpty && double.tryParse(_kelvinController.text)!=null){
      double _kelvin = double.parse(_kelvinController.text);
      double _celsius =  _kelvin-273;
      double _fahrenheit = (_kelvin-273)*1.8+32;

      _celsiusController.text = _celsius.toString();
      _fahrenheitController.text = _fahrenheit.toString();
    }else{
      showDialog(context: context, builder: (context) => AlertDialog(
        title: Text("ALERTA!"),
        content: Text("blob"),
        actions: [TextButton(onPressed: (){
          Navigator.of(context).pop();
        }, child: Text("Ok"))],
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(padding: EdgeInsets.all(25),
        child: Column(children: [
        TextField(
          keyboardType: TextInputType.number,
          decoration: InputDecoration(labelText: "Celsius"),
          onTap: (){
            _fahrenheitController.text = "";
            _kelvinController.text = "";
          },
          controller: _celsiusController,
        ),
        SizedBox(height: _sbh),
        TextField(
          keyboardType: TextInputType.number,
          decoration: InputDecoration(labelText: "Fahrenheit"),
          onTap: (){
            _celsiusController.text = "";
            _kelvinController.text = "";
          },
          controller: _fahrenheitController,
        ),
        SizedBox(height: _sbh),
        TextField(
          keyboardType: TextInputType.number,
          decoration: InputDecoration(labelText: "Kelvin"),
          onTap: (){
            _fahrenheitController.text = "";
            _celsiusController.text = "";
          },
          controller: _kelvinController,
        ),
        SizedBox(height: _sbh),
        ElevatedButton(onPressed: _Calcular, child: Text("Converter"))
      ],),),
    );
  }
}
